let data = require("./2-arrays-logins.cjs");

let secondComponentsArray = [];
data.map((each) => {
  let ipAddress = each.ip_address.split(".");
  secondComponentsArray.push(parseInt(ipAddress[1]));
});

console.log(secondComponentsArray.reduce((acc, val) => acc + val));
