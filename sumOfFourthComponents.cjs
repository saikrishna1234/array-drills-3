let data = require("./2-arrays-logins.cjs");

let fourthComponentsArray = [];
data.map((each) => {
  let ipAddress = each.ip_address.split(".");
  fourthComponentsArray.push(parseInt(ipAddress[3]));
});

console.log(fourthComponentsArray.reduce((acc, val) => acc + val));
