let data = require("./2-arrays-logins.cjs");

function convertStrtoInt(arr) {
  let intArr = [];
  for (let num in arr) {
    intArr.push(parseInt(arr[num]));
  }
  return intArr;
}

let splittedIpAddress = [];
data.map((each) => {
  let ipAddress = each.ip_address;
  let splittedString = ipAddress.split(".");
  each.ip_address = convertStrtoInt(splittedString);
  splittedIpAddress.push(each);
});

console.log(splittedIpAddress);
