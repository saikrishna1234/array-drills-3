let data = require("./2-arrays-logins.cjs");

function sorted(a, b) {
  if (a.first_name < b.first_name) {
    return 1;
  } else if (a.first_name > b.first_name) {
    return -1;
  } else {
    return 0;
  }
}

console.log(data.sort(sorted));
