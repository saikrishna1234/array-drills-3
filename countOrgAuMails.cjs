let data = require("./2-arrays-logins.cjs");

let result = { org_mails: 0, au_mails: 0, com_mails: 0 };

data.map((each) => {
  if (each.email.endsWith(".org")) {
    result["org_mails"] += 1;
  } else if (each.email.endsWith(".au")) {
    result["au_mails"] += 1;
  } else if (each.email.endsWith(".com")) {
    result["com_mails"] += 1;
  }
});

console.log(result);
